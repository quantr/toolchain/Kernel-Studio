import { allowedNodeEnvironmentFlags } from "process";
import * as vscode from 'vscode';
import * as path from 'path';
import { Console } from "console";
import { setPriority } from "os";

export class Simulator {
	context!: vscode.ExtensionContext;
	simulatorPanel!: vscode.WebviewPanel;
	registerPanel!: vscode.WebviewPanel;
	memoryPanel!: vscode.WebviewPanel;
	disassemblerPanel!: vscode.WebviewPanel;
	functionPanel!: vscode.WebviewPanel;
	child: any;
	temp: string = '';
	type: string = '';
	searchINS: boolean = false;
	contFlag: boolean = true;

	start() {
		var cp = require('child_process');
		var str: any;

		const folder = path.join(this.context.extensionPath, 'jar');
		this.child = cp.exec("java -jar " + folder + "/riscv_simulator-1.0.jar --elf " + folder + "/GD32VF103C_START.elf -m 1GB -s 0x0000000 -d dump.json --map=0x0-0x2fffff=0x8000000");
		this.child.stdout.on('data', (data: any) => {
			this.simulatorPanel.webview.postMessage({ type: 'stdout', data: data });

			let lines = data.split('\n');

			for (let x = 0; x < lines.length; x++) {
				let line = lines[x];

				if (line.includes("<registers>")) {
					this.temp = "";
					this.type = "register";
				} else if (line.includes("</registers>")) {
					this.type = "endRegister";
				} else if (line.includes("<memory>")) {
					this.temp = "";
					this.type = "memory";
				} else if (line.includes("</memory>")) {
					this.type = "endMemory";
				} else if (line.includes("<disassembler>")) {
					this.temp = "";
					this.type = "version";
				} else if (line.includes("</disassembler>")) {
					this.type = "endDisassembler";
				} else if (line.includes("<version>")) {
					this.temp = "";
					this.type = "version";
				} else if (line.includes("</version>")) {
					this.type = "endVersion";
				} else if (line.includes("<updateMemoryAddress>")) {
					this.temp = "";
					this.type = "updateMemoryAddress";
				} else if (line.includes("</updateMemoryAddress>")) {
					this.type = "endUpdateMemoryAddress";
				} else if (line.includes("<disasmCur>")) {
					this.temp = "";
					this.type = "disasmCur";
				} else if (line.includes("</disasmCur>")) {
					this.type = "endDisasmCur";
				} else if (line.includes("<matchedMemory>")) {
					this.temp = "";
					this.type = "matchedMemory";
				} else if (line.includes("</matchedMemory>")) {
					this.type = "endmatchedMemory";
				}

				this.temp += line + "\n";

				if (this.type === "endRegister") {
					this.registerPanel.webview.postMessage({ type: "update", xml: this.temp });
					this.functionPanel.webview.postMessage({ type: "update", xml: this.temp });
					this.temp = "";
					this.type = "";
				} else if (this.type === "endMemory") {
					this.memoryPanel.webview.postMessage({ type: "update", xml: this.temp });
					this.temp = "";
					this.type = "";
				} else if (this.type === "endDisassembler") {
					if (!this.searchINS) {
						this.disassemblerPanel.webview.postMessage({ type: "update", xml: this.temp, searchState: false });
					} else {
						this.disassemblerPanel.webview.postMessage({ type: "update", xml: this.temp, searchState: true });
						this.searchINS = false;
					}
					this.temp = "";
					this.type = "";
				} else if (this.type === "endVersion") {
					this.simulatorPanel.webview.postMessage({ type: "version", xml: this.temp });
					this.temp = "";
					this.type = "";
				} else if (this.type === "endUpdateMemoryAddress") {
					this.memoryPanel.webview.postMessage({ type: "lightTheUpdated", xml: this.temp });
					this.temp = "";
					this.type = "";
				} else if (this.type === "endDisasmCur") {
					this.functionPanel.webview.postMessage({ type: "update", xml: this.temp });
					this.temp = "";
					this.type = "";
				} else if (this.type === "endmatchedMemory") {
					this.memoryPanel.webview.postMessage({ type: "updateSearch", xml: this.temp });
					this.temp = "";
					this.type = "";
				}
			}
		});

		this.child.stderr.on('data', (data: any) => {
			console.log(`stderr: ${data}`);
		});

		this.child.on('close', (code: any) => {
			console.log(`RISC-V simulator exited with code ${code}`);
		});
		this.child.stdin.write('xml on\n');
		vscode.window.showInformationMessage('RISC-V simulator started');
	}

	kill() {
		this.child.kill('SIGKILL');
		console.log('SIGKILL');
	}

	registers() {
		this.child.stdin.write('r\n');
	}

	private a() {
		return new Promise<void>((resolve, reject) => { this.child.stdin.write('s\n'); resolve(); });
	}

	step() {
		this.memoryPanel.webview.postMessage({ type: 'initalAddressArray' });
		this.a().then(() => {
			return new Promise<void>((resolve, reject) => { this.loadHistory(); setTimeout(() => resolve(), 500); });
		}).then(() => {
			return new Promise<void>((resolve, reject) => { this.child.stdin.write("x\n"); setTimeout(() => resolve(), 300); });
		}).then(() => {
			return new Promise<void>((resolve, reject) => { this.child.stdin.write("um\n"); resolve(); });
		});

	}

	disassemble(address: string) {
		this.child.stdin.write('disassemble ' + address + ',100\n');
	}

	searchIns(address: string, range: number) {
		this.searchINS = true;
		range = range / 4 + range % 5 + 1;
		this.child.stdin.write(`disassemble ${address},${range}\n`);
	}

	version() {
		this.child.stdin.write('v\n');
	}

	cont() {
		this.child.stdin.write('cont\n');
	}

	private c() {
		return new Promise<void>((resolve, reject) => { this.loadHistory(); setTimeout(() => resolve(), 500); });
	}

	pause() {
		this.memoryPanel.webview.postMessage({ type: 'initalAddressArray' });
		this.c().then(() => {
			return new Promise<void>((resolve, reject) => { this.child.stdin.write("x\n"); setTimeout(() => resolve(), 300); });
		}).then(() => {
			return new Promise<void>((resolve, reject) => { this.child.stdin.write("um\n"); resolve(), 100; });
		}).then(() => {
			return new Promise<void>((resolve, reject) => { this.child.stdin.write('pause\n'); resolve(); });
		});
	}

	loadMemory(address: string) {
		this.child.stdin.write("x /1024 0x" + address + '\n');
	}

	changeMemoryBase(base: string) {
		this.child.stdin.write("x /" + base + '\n');
	}

	private b() {
		return new Promise<void>((resolve, reject) => { this.child.stdin.write("disasm cur\n"); setTimeout(() => resolve(), 300); });
	}

	loadHistory() {
		this.b().then(() => { return new Promise<void>((resolve, reject) => { this.registers(); resolve(); }); });
	}

	searchMemory(startAddress: string, endAddress: string, target: string) {
		this.child.stdin.write(`find ${startAddress},${endAddress},${target}\n`);
	}

	keypress(keyCode: number) {
		if (keyCode === 13) {
			this.child.stdin.write('\n');
			this.simulatorPanel.webview.postMessage({ type: 'stdout', data: '\n' });
		} else if (keyCode === 8) {
			this.simulatorPanel.webview.postMessage({ type: 'stdout', data: 'BS' });
		} else {
			this.child.stdin.write(String.fromCharCode(keyCode));
			this.simulatorPanel.webview.postMessage({ type: 'stdout', data: String.fromCharCode(keyCode) });
		}
	}
}




