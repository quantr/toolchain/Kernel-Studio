import * as vscode from 'vscode';
import { FunctionViewProvider } from './FunctionViewProvider';
import * as path from 'path';
import * as fs from 'fs';
import { Simulator } from './Simulator';
import { measureMemory } from 'vm';

var simulator: Simulator;
var registerPanel: vscode.WebviewPanel | undefined;
var memoryPanel: vscode.WebviewPanel | undefined;
var disassemblerPanel: vscode.WebviewPanel | undefined;
var simulatorPanel: vscode.WebviewPanel | undefined;
var historyPanel: vscode.WebviewPanel | undefined;
var reloadMemoryView: boolean = false;
var reloadRegisterView: boolean = false;
var reloadDisassmeblerView: boolean = false;
var memoryAddress: string;
var updatedMemory: Array<any>;
var smv: string;

export function activate(context: vscode.ExtensionContext) {
	simulator = new Simulator();
	simulator.context = context;

	let memoryViewDisposable = initMemoryView(context);
	context.subscriptions.push(memoryViewDisposable);

	let registerViewDisposable = initRegisterView(context);
	context.subscriptions.push(registerViewDisposable);

	let disassemblerViewDisposable = initDisassemblerView(context);
	context.subscriptions.push(disassemblerViewDisposable);

	let simulatorViewDisposable = initSimulatorView(simulator, context);
	context.subscriptions.push(simulatorViewDisposable);

	let historyViewDisposable = initHistoryView(context);
	context.subscriptions.push(historyViewDisposable);

	initFunctionTreeView();


}

function initFunctionTreeView() {
	const functionViewProvider = new FunctionViewProvider(vscode.workspace.rootPath as string);
	vscode.window.registerTreeDataProvider('functionView', functionViewProvider);
}

function initMemoryView(context: vscode.ExtensionContext) {
	createMemoryPanel(context);
	let memoryViewDisposable = vscode.commands.registerCommand('showMemoryView', () => {
		if (memoryPanel !== undefined) {
			createMemoryPanel(context);
			if (reloadMemoryView) {
				memoryPanel.webview.postMessage({ type: 'stopLoading' });
			}
			memoryPanel.webview.postMessage({ type: 'initial' });
		}
	});
	return memoryViewDisposable;
}

function createMemoryPanel(context: any) {
	memoryPanel = vscode.window.createWebviewPanel(
		'memoryView',
		'Memory',
		vscode.ViewColumn.Three,
		{
			enableScripts: true,
			retainContextWhenHidden: true
		}
	);
	simulator.memoryPanel = memoryPanel;
	memoryPanel.webview.html = loadMemoryView(memoryPanel, context.extensionPath);

	memoryPanel.onDidDispose(
		() => {
			memoryPanel = undefined;
		},
		undefined,
		context.subscriptions
	);
	memoryPanel.webview.onDidReceiveMessage(
		message => {
			switch (message.command) {
				case 'getMemory':
					simulator.loadMemory(message.text);
					return;
				case 'changeBase':
					simulator.changeMemoryBase(message.text);
					return;
				case 'next':
					memoryAddress = message.text;

					updatedMemory = message.array;

					if (memoryPanel !== undefined) {
						memoryPanel.webview.html = loadSearchMemoryView(memoryPanel, context.extensionPath);
						memoryPanel.webview.postMessage({ type: 'ddd', text: smv });
					}
					return;
				case 'back':
					smv = message.text;
					console.log(smv);
					updatedMemory = message.array;
					if (memoryPanel !== undefined) {
						memoryPanel.webview.html = loadMemoryView(memoryPanel, context.extensionPath);
						memoryPanel.webview.postMessage({ type: 'stopLoading' });
						memoryPanel.webview.postMessage({ type: 'ddd', text: memoryAddress, array: updatedMemory });
					}
					return;
				case 'searchMemory':
					var data = message.text.split(",");
					console.log("fk");
					simulator.searchMemory(data[1], data[2], data[0]);
					return;
				case "dadad":
					vscode.window.showInformationMessage('getFunc1tion: ' + message.text);

					return;
			}
		},
		undefined,
		context.subscriptions
	);
}

function loadMemoryView(panel: vscode.WebviewPanel, extensionPath: string) {
	const resourcePath = path.join(extensionPath, 'src', 'webview', 'memoryView.html');
	let html = fs.readFileSync(resourcePath, 'utf-8');

	//const bootstrapSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview', 'bootstrap-4.5.2-dist', 'css', 'bootstrap.css')));
	// html = html.replace(/{bootstrapPath}/g, bootstrapSrc.toString());
	// console.log('bootstrapSrc.toString()=' + bootstrapSrc.toString());
	const basePathSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview')));
	html = html.replace(/{basePath}/g, basePathSrc.toString());

	return html;
}

function loadSearchMemoryView(panel: vscode.WebviewPanel | undefined, extensionPath: string) {
	const resourcePath = path.join(extensionPath, 'src', 'webview', 'memorySearchView.html');
	let html = fs.readFileSync(resourcePath, 'utf-8');

	if (panel !== undefined) {
		const basePathSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview')));
		html = html.replace(/{basePath}/g, basePathSrc.toString());
	}
	return html;
}

function initRegisterView(context: vscode.ExtensionContext) {
	// let registerPanel: vscode.WebviewPanel | undefined = undefined;
	createRegisterPanel(context);
	let registerViewDisposable = vscode.commands.registerCommand('showRegisterView', () => {

		createRegisterPanel(context);
		if (reloadRegisterView && registerPanel !== undefined) {
			registerPanel.webview.postMessage({ type: 'stopLoading' });
			simulator.registers();
		}
	});
	return registerViewDisposable;
}

function createRegisterPanel(context: any) {
	registerPanel = vscode.window.createWebviewPanel(
		'registerView',
		'Registers',
		vscode.ViewColumn.Three,
		{
			enableScripts: true,
			retainContextWhenHidden: true
		}
	);
	simulator.registerPanel = registerPanel;
	registerPanel.webview.html = loadRegisterView(registerPanel, context.extensionPath);

	registerPanel.onDidDispose(
		() => {
			registerPanel = undefined;
		},
		undefined,
		context.subscriptions
	);
	registerPanel.webview.onDidReceiveMessage(
		message => {
			switch (message.command) {
				case 'getRegister':
					vscode.window.showInformationMessage('getRegister: ' + message.text);
					return;
			}
		},
		undefined,
		context.subscriptions
	);
}

function loadRegisterView(panel: vscode.WebviewPanel, extensionPath: string) {
	const resourcePath = path.join(extensionPath, 'src', 'webview', 'registerView.html');
	let html = fs.readFileSync(resourcePath, 'utf-8');

	const bootstrapSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview', 'bootstrap-4.5.2-dist', 'css', 'bootstrap.css')));
	const basePathSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview')));
	html = html.replace(/{basePath}/g, basePathSrc.toString());
	console.log(html);
	return html;
}


function initDisassemblerView(context: vscode.ExtensionContext) {
	createDisassemblerPanel(context);
	let disassemblerViewDisposable = vscode.commands.registerCommand('showDisassemblerView', () => {

		createDisassemblerPanel(context);
		if (reloadDisassmeblerView && disassemblerPanel !== undefined) {
			disassemblerPanel.webview.postMessage({ type: 'start' });
		}
	});
	return disassemblerViewDisposable;
}

function createDisassemblerPanel(context: any) {
	disassemblerPanel = vscode.window.createWebviewPanel(
		'disassemblerView',
		'Disassembler',
		vscode.ViewColumn.Two,
		{
			enableScripts: true,
			retainContextWhenHidden: true
		}
	);
	simulator.disassemblerPanel = disassemblerPanel;
	disassemblerPanel.webview.html = loadDisassemblerView(disassemblerPanel, context.extensionPath);

	disassemblerPanel.onDidDispose(
		() => {
			disassemblerPanel = undefined;
		},
		undefined,
		context.subscriptions
	);
	disassemblerPanel.webview.onDidReceiveMessage(
		message => {
			switch (message.command) {
				case 'disassemble':
					simulator.disassemble(message.address);
					return;
				case 'search':
					simulator.searchIns(message.address, message.range);
					return;
			}
		},
		undefined,
		context.subscriptions
	);
}

function loadDisassemblerView(panel: vscode.WebviewPanel, extensionPath: string) {
	const resourcePath = path.join(extensionPath, 'src', 'webview', 'disassemblerView.html');
	let html = fs.readFileSync(resourcePath, 'utf-8');

	const bootstrapSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview', 'bootstrap-4.5.2-dist', 'css', 'bootstrap.css')));
	const basePathSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview')));
	html = html.replace(/{basePath}/g, basePathSrc.toString());
	return html;
}

function initSimulatorView(simulator: Simulator, context: vscode.ExtensionContext) {
	createSimulatorPanel(simulator, context);
	let memoryViewDisposable = vscode.commands.registerCommand('showSimulatorView', () => {
		createSimulatorPanel(simulator, context);
	});
	return memoryViewDisposable;
}

function st() {
	return new Promise<void>((resolve, reject) => { simulator.start(); setTimeout(() => resolve(), 500); });
}

function createSimulatorPanel(simulator: Simulator, context: any) {
	simulatorPanel = vscode.window.createWebviewPanel(
		'simulatorView',
		'Simulator',
		vscode.ViewColumn.Two,
		{
			enableScripts: true,
			retainContextWhenHidden: true
		}
	);
	simulator.simulatorPanel = simulatorPanel;
	simulatorPanel.webview.html = loadSimulatorView(simulatorPanel, context.extensionPath);

	simulatorPanel.onDidDispose(
		() => {
			simulatorPanel = undefined;
		},
		undefined,
		context.subscriptions
	);
	simulatorPanel.webview.onDidReceiveMessage(
		message => {
			console.log('message.command=' + message.command);
			switch (message.command) {
				case 'start':
					//simulator.start();
					st().then(() => {
						return new Promise<void>((resolve, reject) => { simulator.loadMemory("0"); setTimeout(() => resolve(), 1000); });
					}).then(() => {
						return new Promise<void>((resolve, reject) => { simulator.loadHistory(); resolve(); });
					});
					reloadRegisterView = true;
					reloadMemoryView = true;
					reloadDisassmeblerView = true;
					if (registerPanel) {
						registerPanel.webview.postMessage({ type: 'stopLoading' });
					}
					if (memoryPanel) {
						memoryPanel.webview.postMessage({ type: 'stopLoading' });
					}
					if (disassemblerPanel) {
						disassemblerPanel.webview.postMessage({ type: 'start' });
						simulator.disassemble("0x8000000");
					}
					if (historyPanel) {
						historyPanel.webview.postMessage({ type: 'stopLoading' });
					}
					break;
				case 'kill':
					simulator.kill();
					break;
				case 'step':
					simulator.step();
					break;
				case 'registers':
					const columnToShowIn = vscode.window.activeTextEditor
						? vscode.window.activeTextEditor.viewColumn
						: undefined;
					if (registerPanel) {
						registerPanel.reveal(columnToShowIn);
					}
					simulator.registers();
					break;
				case 'version':
					simulator.version();
					break;
				case 'cont':
					simulator.cont();
					break;
				case 'pause':
					simulator.pause();
					break;
				case 'keypress':
					simulator.keypress(message.keyCode);
					break;
			}
		},
		undefined,
		context.subscriptions
	);
}

function loadSimulatorView(panel: vscode.WebviewPanel, extensionPath: string) {
	const resourcePath = path.join(extensionPath, 'src', 'webview', 'simulatorView.html');
	let html = fs.readFileSync(resourcePath, 'utf-8');

	const bootstrapSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview', 'bootstrap-4.5.2-dist', 'css', 'bootstrap.css')));
	const basePathSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview')));
	html = html.replace(/{basePath}/g, basePathSrc.toString());

	return html;
}

function initHistoryView(context: vscode.ExtensionContext) {
	createHistoryPanel(context);
	let historyViewDisposable = vscode.commands.registerCommand('showHistoryView', () => {
		createHistoryPanel(context);
	});
	return historyViewDisposable;
}

function createHistoryPanel(context: any) {
	historyPanel = vscode.window.createWebviewPanel(
		'HistoryView',
		'History',
		vscode.ViewColumn.One,
		{
			enableScripts: true,
			retainContextWhenHidden: true
		}
	);
	simulator.functionPanel = historyPanel;
	historyPanel.webview.html = loadHistoryView(historyPanel, context.extensionPath);

	historyPanel.onDidDispose(
		() => {
			historyPanel = undefined;
		},
		undefined,
		context.subscriptions
	);
	historyPanel.webview.onDidReceiveMessage(
		message => {
			switch (message.command) {
				case 'wtf':
					vscode.window.showInformationMessage('getFunc1tion: ' + message.text);
					return;
			}
		},
		undefined,
		context.subscriptions
	);
}

function loadHistoryView(panel: vscode.WebviewPanel, extensionPath: string) {
	const resourcePath = path.join(extensionPath, 'src', 'webview', 'historyView.html');
	let html = fs.readFileSync(resourcePath, 'utf-8');

	//const bootstrapSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview', 'bootstrap-4.5.2-dist', 'css', 'bootstrap.css')));
	// html = html.replace(/{bootstrapPath}/g, bootstrapSrc.toString());
	// console.log('bootstrapSrc.toString()=' + bootstrapSrc.toString());
	const basePathSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview')));
	html = html.replace(/{basePath}/g, basePathSrc.toString());

	return html;
}

export function deactivate() { }
